import {put, call, delay} from "redux-saga/effects";
import * as actions from "../../store/actions";
import axios from "../../axios-orders";

export function* logoutSaga(action) {
    yield call([localStorage, "removeItem"], "token");  //call makes generator functions easy to mock & test
    yield call([localStorage, "removeItem"], "expirationDate");
    yield call([localStorage, "removeItem"], "userId");
    yield put(actions.logoutSuccess());
}

export function* checkAuthTimeoutSaga(action)  {
    yield delay(action.expirationTime * 1000); //setTimeout expects real seconds vs milliseconds returned by expiresIn
    yield put(actions.logout());
}

export function * authUserSaga(action) {
    yield put(actions.authStart());
    const authData = {
        email: action.email,
        password: action.password,
        returnSecureToken: true
    }
    let url = 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyDmlek0mCFVkXnEH03-FOcdIFDAnAXSWqA';
    if(!action.isSignup){
        url = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyDmlek0mCFVkXnEH03-FOcdIFDAnAXSWqA';
    }
    const response = yield call([axios, "post"], url, authData);  //can use call with for axios calls too
    try {
        const expirationDate = yield new Date(new Date().getTime() + response.data.expiresIn * 1000);
        yield localStorage.setItem('token', response.data.idToken);  //localStorage is a var built into Javascript. Yield isn't necessary as localStorage is a synchronous action but leave to be consistent in code
        yield localStorage.setItem('expirationDate', expirationDate.toString());
        yield localStorage.setItem('userId', response.data.localId);
        yield put(actions.authSuccess(response.data.idToken, response.data.localId));
        yield put(actions.checkAuthTimeout(response.data.expiresIn));
    } catch (error) {
        yield put(actions.authFail(error.response.data.error));
    }
}

export function* authCheckStateSaga(action) {
        const token = yield localStorage.getItem('token');
        if (!token) {
            yield  put(actions.logout());
        }else {
            const expirationDate = yield new Date(localStorage.getItem('expirationDate'));
            if(expirationDate <= new Date()){
                put(actions.logout());
            } else {
                const userId = yield localStorage.getItem('userId');
                yield put (actions.authSuccess(token, userId));
                yield put(actions.checkAuthTimeout((expirationDate.getTime() - new Date().getTime())/1000));
            }

        }
}