
import * as actionTypes from "../actions/actionTypes";
import {authCheckStateSaga, authUserSaga, checkAuthTimeoutSaga, logoutSaga} from "./auth";
import {initIngredientsSaga} from "./burgerBuilder";
import {fetchOrdersSaga, purchaseBurgerSaga} from "./order";
import {takeEvery, takeLatest, all} from "redux-saga/effects";

export function* watchAuth() {
    yield all([  //group yields together to run concurrently
        takeEvery(actionTypes.AUTH_INITIATE_LOGOUT, logoutSaga),  //listen to action & execute saga
        takeEvery(actionTypes.AUTH_CHECK_TIMEOUT, checkAuthTimeoutSaga),
        takeEvery(actionTypes.AUTH_USER, authUserSaga),
        takeEvery(actionTypes.AUTH_CHECK_STATE, authCheckStateSaga),
    ]);
}

export function* watchBurgerBuilder() {
    yield takeEvery(actionTypes.INIT_INGREDIENTS, initIngredientsSaga);
}

export function* watchOrder() {
    yield takeLatest(actionTypes.PURCHASE_BURGER, purchaseBurgerSaga);  //takeLatest will only take the last button click to purchase
    yield takeEvery(actionTypes.FETCH_ORDERS, fetchOrdersSaga);
}