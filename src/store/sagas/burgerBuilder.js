import axios from "../../axios-orders";
import {put} from "redux-saga/effects";
import * as actions from "../actions";

export function* initIngredientsSaga(action) {
    //axios.get('https://react-my-burger-df294.firebaseio.com/ingredients')  //bad url for testing ingredients loading error
    const response = yield axios.get('https://react-my-burger-df294.firebaseio.com/ingredients.json');
    try {
        yield put(actions.setIngredients(response.data)); //async code calls sync code
    } catch (error) {
        yield put(actions.fetchIngredientsFailed());
    }
}