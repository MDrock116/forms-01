import * as actionTypes from '../actions/actionTypes';
import {updateObject} from "../../shared/utility";

const initialState = {
    ingredients: null,
    totalPrice: 4,
    error: false,
    building: false
}

const INGREDIENT_PRICES = {
    salad: 0.5,
    cheese: 0.4,
    meat: 1.3,
    bacon: 0.7
};


const addIngredient = (action, state) => {
    const updatedIngredient1 = {[action.ingredientName]: state.ingredients[action.ingredientName] + 1};
    const updatedIngredients1 = updateObject(state.ingredients, updatedIngredient1);
    const updatedState1 = {
        ingredients: updatedIngredients1,
        totalPrice: state.totalPrice + INGREDIENT_PRICES[action.ingredientName],
        building: true
    }
    return updateObject(state, updatedState1);
}

const removeIngredient = (action, state) => {
    const updatedIngredient2 = {[action.ingredientName]: state.ingredients[action.ingredientName] - 1};
    const updatedIngredients2 = updateObject(state.ingredients, updatedIngredient2);
    const updatedState2 = {
        ingredients: updatedIngredients2,
        totalPrice: state.totalPrice - INGREDIENT_PRICES[action.ingredientName],
        building: true
    }
    return updateObject(state, updatedState2);
}

const setIngredients = (state, action) => {
    return updateObject(state, {
        ingredients: action.ingredients, //in a previous lecture I chose not to hardcode my ingredients to reorder them as he did
        totalPrice: 4,
        error: false,
        building: false
    })
}

const fetchIngredientsFailed = (state) => {
    return updateObject(state, {error: true});
}

const burgerBuilderReducer = (state=initialState, action) => {
    switch (action.type) {
        case actionTypes.SET_INGREDIENTS: return setIngredients(state, action);
        case actionTypes.FETCH_INGREDIENTS_FAILED: return fetchIngredientsFailed(state);
        case actionTypes.ADD_INGREDIENTS: return addIngredient(action, state);
        case actionTypes.REMOVE_INGREDIENTS: return removeIngredient(action, state);
        default: return state;
    }
};

export default burgerBuilderReducer;