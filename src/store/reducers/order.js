import * as actionTypes from '../actions/actionTypes';
import {updateObject} from "../../shared/utility";

const initialState = {
    orders: [],
    loading: false,
    purchased: false
}

const purchaseBurgerSuccess = (action, state) => {
    const newOrder = updateObject(action.orderData, {id: action.orderId});
    return updateObject(state, {
        loading: false,
        orders: state.orders.concat(newOrder), //no longer needed now that we load orders from server when loading Orders page
        purchased: true
    });
}

const purchaseBurgerStart = (state) => {
    return updateObject(state, {loading: true});
}

const purchaseBurgerInit = (state) => {
    return updateObject(state, {purchased: false});
}

const purchaseBurgerFail = (state) => {
    return updateObject(state, {loading: false});
}

const fetchOrderStart = (state) => {
    return updateObject(state, {loading: true});
}

const fetchOrderSuccess = (state, action) => {
    return updateObject(state, {
        orders: action.orders,
        loading: false
    });
}

const fetchOrdersFailed = (state) => {
    return updateObject(state, {loading: false});
}

const orderReducer = (state=initialState, action) => {
    switch (action.type) {
        case actionTypes.PURCHASE_BURGER_INIT: return purchaseBurgerInit(state);
        case actionTypes.PURCHASE_BURGER_START: return purchaseBurgerStart(state);
        case actionTypes.PURCHASE_BURGER_SUCCESS: return purchaseBurgerSuccess(action, state);
        case actionTypes.PURCHASE_BURGER_FAIL: return purchaseBurgerFail(state);
        case actionTypes.FETCH_ORDERS_START: return fetchOrderStart(state);
        case actionTypes.FETCH_ORDERS_SUCCESS: return fetchOrderSuccess(state, action);
        case actionTypes.FETCH_ORDERS_FAILED: return fetchOrdersFailed(state);
        default: return state;
    }
};

export default orderReducer;