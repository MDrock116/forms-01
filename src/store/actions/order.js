//Action Creator
import * as actionTypes from "./actionTypes";

export const purchaseBurgerSuccess= (id, orderData) => {
    return {
        type: actionTypes.PURCHASE_BURGER_SUCCESS,
        orderId: id,
        orderData: orderData
    };
}

export const purchaseBurgerFail= (error) => {
    return {
        type: actionTypes.PURCHASE_BURGER_FAIL,
        error: error
    };
}

export const purchaseBurgerStart= () => {
    return {
        type: actionTypes.PURCHASE_BURGER_START,
    };
}

export const purchaseBurger=  (orderData, token) => {   //executed after clicking 'ORDER' on ContactData
    return {
        type: actionTypes.PURCHASE_BURGER,
        orderData,
        token
    }
}

export const purchaseInit = () => {   //executed after clicking 'CONTINUE' on OrderSummary
    return {
        type: actionTypes.PURCHASE_BURGER_INIT
    };
}

export const fetchOrdersSuccess= (orders) => {
    return {
        type: actionTypes.FETCH_ORDERS_SUCCESS,
        orders: orders
    };
}

export const fetchOrdersFail= (error) => {
    return {
        type: actionTypes.FETCH_ORDERS_FAILED,
        error: error
    };
}

export const fetchOrdersStart= () => {
    return {
        type: actionTypes.FETCH_ORDERS_START,
    };
}

export const fetchOrders=  (token, userId) => {
    return {
        type: actionTypes.FETCH_ORDERS,
        token,
        userId
    }
}