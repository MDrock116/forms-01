import React, {useEffect, Suspense} from 'react';
import {Redirect, Route, Switch, withRouter} from 'react-router-dom';

import Layout from './hoc/Layout/Layout';
import BurgerBuilder from './containers/BurgerBuilder/BurgerBuilder';
import Logout from "./containers/Auth/Logout/Logout";
import * as actions from "./store/actions";
import {connect} from "react-redux";

const Auth = React.lazy(() => {
    return import('./containers/Auth/Auth');
});

const Orders = React.lazy(() => {
    return import('./containers/Orders/Orders');
});

const Checkout = React.lazy(() => {
    return import('./containers/Checkout/Checkout');
});


const app = props => {
    const {onTryAutoSignup} = props;
    useEffect(() => {
        onTryAutoSignup();  //should probably be named onTryAutoLogin
    }, [onTryAutoSignup]);
        //funct's are objects. whenever code defining funct reruns a new funct is created & referenced by this variable
        //though this won't happen in our case, in theory, we should follow this rule for defining useEffect's as in some code it could




    let routes = (
        <Switch>
          <Route path="/auth" exact render={(props) => <Auth {...props} />} />  {/*React.lazy returns a JSX, not a component so it needs render method*/}
          <Route path="/" exact component={BurgerBuilder} />
          <Redirect to="/" />  {/*redirect to BurgerBuilder for any other path request */}
        </Switch>
    );

    if(props.isAuthenticated){
      routes = (
          <Switch>
            <Route path="/checkout" render={(props) => <Checkout {...props} />} />
            <Route path="/orders" render={(props) => <Orders {...props} />} />
            <Route path="/logout" exact component={Logout}  />
            <Route path="/auth" render={(props) => <Auth {...props} />} />  {/*though this allows already authenticated users to view the auth page, it's necessary to send users that authenticate in the middle of an order to auth page as that page will redirect them back to where they left off in their order*/}
            <Route path="/" exact component={BurgerBuilder} />
            <Redirect to="/" />  {/*redirect to BurgerBuilder for any other path request */}
          </Switch>
      )
    }

    return (
      <div>
        <Layout>
            <Suspense fallback={<div>loading...</div> }>{routes}</Suspense>
        </Layout>
      </div>
    );
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignup: () => dispatch(actions.authCheckState())
  }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(app));  //'withRouter' allows you to wrap a component which includes <Route ...>'s with 'connect'