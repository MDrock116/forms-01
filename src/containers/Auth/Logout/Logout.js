import React, {useEffect} from 'react';
import * as authActions from "../../../store/actions";
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";

const logout = props => {
    const {onLogout} = props;

    useEffect(() => {
        onLogout();
    }, [onLogout])


    return <Redirect to="/" />

}

const mapDispatchToProps = dispatch => {
    return {
        onLogout: () => dispatch(authActions.logout())
    }
};

export default connect(null, mapDispatchToProps)(logout);
