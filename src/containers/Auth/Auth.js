import React, {useEffect, useState} from 'react';
import Input from '../../components/UI/Input/input';
import Button from '../../components/UI/Button/Button';
import classes from './Auth.css';
import * as authActions from "../../store/actions";
import {connect} from "react-redux";
import Spinner from "../../components/UI/Spinner/Spinner";
import * as actions from "../../store/actions";
import {Redirect} from "react-router-dom";
import {checkValidity, updateObject} from "../../shared/utility";


const auth = props => {
    const [authForm, setAuthForm] = useState({
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Mail Address'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Password'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 6
                },
                valid: false,
                touched: false
            }
    });

    const [isSignup, setIsSignup] = useState(true);

    const {buildingBurger, authRedirectPath, onSetAuthRedirectPath} = props;

    useEffect(()=> {
        if(!buildingBurger  && authRedirectPath !== '/'){
            onSetAuthRedirectPath();
        }
    },[buildingBurger, authRedirectPath, onSetAuthRedirectPath])

    const inputChangedHandler = (event, controlName) => {
        const updatedAuthForm = updateObject(authForm, {
            [controlName]: updateObject(authForm[controlName], {
                value: event.target.value,
                valid: checkValidity(event.target.value, authForm[controlName].validation),
                touched: true
            })
        });

        setAuthForm(updatedAuthForm);  //replace state with root copy
    }

    const submitHandler = ( event ) => {
        event.preventDefault();
        props.onAuth(
            authForm.email.value,
            authForm.password.value,
            isSignup);
    }

    const switchAuthModeHandler = () => {
        setIsSignup(!isSignup);
    }

    const formElementArray = [];
    for(let key in authForm) //key= email, password
            formElementArray.push({
                id: key,
                config: authForm[key] //config will contain: elementType, elementConfig, value, etc...
            })

    let form = (
            <form onSubmit={submitHandler}>
                {formElementArray.map(formElement => (
                    <Input
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.value}
                        invalid={!formElement.config.valid}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        changed={(event) => inputChangedHandler(event, formElement.id)}
                    />
                ))}
            </form>
    );

    if (props.loading) {
            form = <Spinner />;
    }

    let errorMessage = null;

    if(props.error){
            errorMessage = (
                <p>{props.error.message}</p>
            )
    }

    const purchasedRedirect = props.isAuthenticated
            ? <Redirect to = {props.authRedirectPath} />
            : null;

    return (
            <div className={classes.Auth}>
                {purchasedRedirect}
                {errorMessage}
                <form onSubmit={submitHandler}>
                    {form}
                    <Button btnType="Success" >SUBMIT</Button>
                    <Button
                        notSubmitButton={true}
                        clicked={switchAuthModeHandler}
                        btnType="Danger" >SWITCH TO {isSignup ? 'SIGN-IN' : 'SIGN-UP'}</Button>
                </form>
            </div>
    );
}

const mapStateToProps = state => {
    return {
        loading: state.auth.loading,
        error: state.auth.error,
        isAuthenticated: state.auth.token !== null,
        buildingBurger: state.bbr.building,
        authRedirectPath: state.auth.authRedirectPath
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAuth: (email, password, isSignup) => dispatch(authActions.auth(email, password, isSignup)),
        onSetAuthRedirectPath: () => dispatch(actions.setAuthRedirectPath('/'))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(auth);
