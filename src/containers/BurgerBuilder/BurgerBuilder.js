import React, {useCallback, useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';

import Auxy from '../../hoc/Auxy/Auxy';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import axios from '../../axios-orders';
import * as actions from "../../store/actions";  //here, index.js is default folder under actions

const burgerBuilder = props => {

    const [purchasing, setPurchasing] = useState(false); //turns TRUE after OrderNow button clicked, which triggers OrderSummary Modal to display

    const dispatch = useDispatch();
    const ings = useSelector(state => state.bbr.ingredients);
    const price = useSelector(state => state.bbr.totalPrice);
    const error = useSelector(state => state.bbr.error);
    const isAuthenticated = useSelector( state => state.auth.token !== null);

    const onIngredientsAdded = (ingName) => dispatch(actions.addIngredient(ingName));
    const onIngredientsRemove = (ingName) => dispatch(actions.removeIngredient(ingName));
    const onInitIngredients = useCallback(() => dispatch(actions.initIngredients()), []); //useCallback ensures that onInitIngredients will cache the dispatch function & not recreate it when useEffect is called below.
    const onInitPurchase = () => dispatch(actions.purchaseInit());
    const onSetAuthRedirectPath = (path) => dispatch(actions.setAuthRedirectPath(path));

    useEffect(() => {
        onInitIngredients();  //with mapDispatchToProps this function was wrapped in a dispatch, thus cached automatically by redux to prevent an infinite loop.  So, now that the this function is on its own we use useCallback() above to manually prevent it
    }, [onInitIngredients]);

    const updatePurchaseState = ingredients => {  //toggle OrderNow button (when at least 1 ingredient is added to burger
        const sum = Object.keys( ingredients )
            .map( igKey => {
                return ingredients[igKey];
            } )
            .reduce( ( sum, el ) => {
                return sum + el;
            }, 0 );
        return sum > 0;
    }

    const purchaseHandler = () => {
        if(isAuthenticated) {
            setPurchasing(true);
        }else{
            onSetAuthRedirectPath('/checkout');
            props.history.push('/auth');
        }
    }

    const purchaseCancelHandler = () => {
        setPurchasing(false);
    }

    const purchaseContinueHandler = () => {
        props.history.push('/checkout');
        onInitPurchase();
    }

        const disabledInfo = {  //a copy of ingredients object passed to BuildControls disabled prop
            ...ings
        };
        for ( let key in disabledInfo ) {
            disabledInfo[key] = disabledInfo[key] <= 0
        }
        let orderSummary = null;
        let burger = error ? <p>Ingredients can't be loaded!</p> : <Spinner />;

        if ( ings ) {
            burger = (
                <Auxy>
                    <Burger ingredients={ings} />
                    <BuildControls
                        ingredientAdded={onIngredientsAdded}
                        ingredientRemoved={onIngredientsRemove}
                        disabled={disabledInfo}
                        purchasable={updatePurchaseState(ings)}  //passed as funct call vs funct to force execution on re-render
                        ordered={purchaseHandler}
                        isAuth = {isAuthenticated}
                        price={price} />
                </Auxy>
            );
            orderSummary = <OrderSummary
                ingredients={ings}
                price={price}
                purchaseCancelled={purchaseCancelHandler}
                purchaseContinued={purchaseContinueHandler} />;
        }

        // {salad: true, meat: false, ...}
        return (
            <Auxy>
                <Modal show={purchasing} modalClosed={purchaseCancelHandler}>
                    {orderSummary}
                </Modal>
                {burger}
            </Auxy>
        );
}

export default withErrorHandler( burgerBuilder, axios );