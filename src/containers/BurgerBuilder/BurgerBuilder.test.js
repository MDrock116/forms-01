import React from "react";

import {configure, shallow} from "enzyme";
import Adapter from 'enzyme-adapter-react-16'

import {BurgerBuilder} from "./BurgerBuilder";
import BuildControls from "../../components/Burger/BuildControls/BuildControls";

configure({adapter: new Adapter()});

describe('<BurgerBuilder />', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = shallow(<BurgerBuilder onInitIngredients={()=>{}} />); //during BurgerBuilder's shallow creation it is rendered, which calls componentDidMount().  onInitIngredients is referenced in that function but undefined (as mapDispatchToProp is not called in shallow creation), so it needs to be stubbed
    })

    it('should render <BuildControls /> when receiving ingredients',  () => {
        wrapper.setProps({ings: {salad: 0}});
        expect(wrapper.find(BuildControls)).toHaveLength(1);
        //expect(wrapper.contains(<NavigationItem link="/logout">Logout</NavigationItem>)).toEqual(true);
    })
})
