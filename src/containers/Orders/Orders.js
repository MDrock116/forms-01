import React, {useCallback, useEffect} from 'react';

import Order from '../../components/Order/Order';
import axios from '../../axios-orders';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import * as actions from "../../store/actions";
import {useDispatch, useSelector} from "react-redux";
import Spinner from '../../components/UI/Spinner/Spinner';

const orders = props => {
    const orders = useSelector(state => state.order.orders);
    const loading = useSelector(state => state.order.loading);
    const token = useSelector(state => state.auth.token);
    const userId = useSelector(state => state.auth.userId);

    const dispatch = useDispatch();
    const onOrdersFetch = useCallback((token, userId) => dispatch(actions.fetchOrders(token, userId)), []);


    useEffect(() => {
        onOrdersFetch(token, userId);
    }, [onOrdersFetch])

        let ordersResult =  <Spinner />;

        if (!loading) {
            ordersResult = orders.map(order => (
                <Order
                    key={order.id}
                    ingredients={order.ingredients}
                    price={order.price} />
            ))
        }
        return (
            <div>
                {ordersResult}
            </div>
        );
}

export default withErrorHandler( orders, axios );