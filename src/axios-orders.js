import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://react-my-burger-df294.firebaseio.com/'
});

export default instance;