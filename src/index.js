import React from 'react';
import ReactDOM from 'react-dom';
import {applyMiddleware, compose, createStore} from 'redux';
import { Provider} from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import createSagaMiddleware from "redux-saga";

import './index.css';
import App from './App';
import reducer from './store/reducers/rootReducer';
import registerServiceWorker from './registerServiceWorker';
import {watchAuth, watchBurgerBuilder, watchOrder} from "./store/sagas";

const composeEnhancers = process.env.NODE_ENV === 'development' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null || compose

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
    reducer,
    composeEnhancers(applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(watchAuth);
sagaMiddleware.run(watchBurgerBuilder);
sagaMiddleware.run(watchOrder);

const app = (
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>
);

ReactDOM.render( app, document.getElementById( 'root' ) );
registerServiceWorker();





