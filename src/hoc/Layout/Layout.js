import React, {useState} from 'react';

import Auxy from '../Auxy/Auxy';
import classes from './Layout.css';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer';
import {connect} from "react-redux";

const layout = props => {
    const [sideDrawerIsVisible, setSideDrawIsVisible] = useState(false);

    const sideDrawerClosedHandler = () => {
        setSideDrawIsVisible(false);
    }

    const sideDrawerToggleHandler = () => {
        setSideDrawIsVisible(!sideDrawerIsVisible)
    }

    return (
        <Auxy>
            <Toolbar
                isAuth={props.isAuthenticated}
                drawerToggleClicked={sideDrawerToggleHandler}/>
            <SideDrawer
                isAuth={props.isAuthenticated}
                open={sideDrawerIsVisible}
                closed={sideDrawerClosedHandler}/>
            <main className={classes.Content}>
                {props.children}
            </main>
        </Auxy>
    );
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.token !== null
    };
};

export default connect(mapStateToProps)(layout);